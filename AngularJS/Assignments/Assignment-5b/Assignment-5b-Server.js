var express = require('express');
var app = new express();

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/Assignment-5b.html");
    console.log(req.url);
});

app.get('/angular.js', function (req, res) {
    res.sendFile(__dirname + "/angular.js");
    console.log(req.url);
});

app.get('/Assignment-5b-Technical.html', function (req, res) {
    res.sendFile(__dirname + "/Assignment-5b-Technical.html");
    console.log(req.url);
});

app.get('/Assignment-5b-NonTechnical.html', function (req, res) {
    res.sendFile(__dirname + "/Assignment-5b-NonTechnical.html");
    console.log(req.url);
});

app.listen(8000, function () {
    console.log('Server listening to 8000');
});
