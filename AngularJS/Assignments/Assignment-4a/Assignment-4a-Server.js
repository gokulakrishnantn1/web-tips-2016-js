var express = require('express');
var app = new express();

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/Assignment-4a.html");
});

app.get('/angular.js', function (req, res) {
    res.sendFile(__dirname + "/angular.js");
});

app.get('/Assignment-4a-Template.html', function (req, res) {
    res.sendFile(__dirname + "/Assignment-4a-Template.html");
});

app.listen(8000, function () {
    console.log('Server listening to 8000');
});
