var express = require('express');
var app = new express();

app.get('/', function (req, res) {
    res.sendFile(__dirname + "/Assignment-5a.html");
    console.log(req.url);
});

app.get('/angular.js', function (req, res) {
    res.sendFile(__dirname + "/angular.js");
    console.log(req.url);
});

app.get('/Assignment-5a-Technical.html', function (req, res) {
    res.sendFile(__dirname + "/Assignment-5a-Technical.html");
    console.log(req.url);
});

app.get('/Assignment-5a-NonTechnical.html', function (req, res) {
    res.sendFile(__dirname + "/Assignment-5a-NonTechnical.html");
    console.log(req.url);
});

app.listen(8000, function () {
    console.log('Server listening to 8000');
});
