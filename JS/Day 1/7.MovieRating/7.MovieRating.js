var movArr = {};
movArr.list = [{name:"Assassin",rating:4},{name:"Body Of Lies",rating:5},{name:"Conjuring",rating:3}];
movArr.highest = function()
{
    var rate=0;
    var movie=[];
    for (var each in movArr.list)
        if(rate<=movArr.list[each].rating)
            rate=movArr.list[each].rating;
    for (var each in movArr.list)
    {
        if(rate == movArr.list[each].rating)
            movie.push(movArr.list[each].name);
    }
    return movie;
};


function list()
{
    var movies = "";
    for(var i=0; i<movArr.list.length; i++)
    {
        for(var j=i+1; j<movArr.list.length; j++)
        {
            var temp;
            if(movArr.list[i].rating < movArr.list[j].rating)
            {
                temp = movArr.list[i];
                movArr.list[i] = movArr.list[j];
                movArr.list[j] = temp;
            }
        }
        movies += "<p>"+(i+1)+". "+movArr.list[i].name+" - "+movArr.list[i].rating+"</p>";
    }
    document.getElementById("leftHead").innerHTML = "Movie List"+movies;
    if( movArr.highest().length == 0 )
        document.getElementById("topMovie").innerHTML = "";
    else if( movArr.highest().length == 1 )
        document.getElementById("topMovie").innerHTML = 'The Top Rated movie is "'+movArr.highest()+'"';
    else
    {
        document.getElementById("topMovie").innerHTML = "Top Rated movies";
        for( var i=0; i<movArr.highest().length; i++ )
            document.getElementById("topMovie").innerHTML += "<p>"+(i+1)+". "+movArr.highest()[i]+"</p>";
    }
}

function add()
{
    var name = document.getElementById("name").value, rating = document.getElementById("rating").value;
    if( name=="" || rating=="" )
        alert("Fields cannot be empty");
    else
    {
        var flag=1;
        for(var i=0; i<movArr.list.length; i++)
            if( name == movArr.list[i].name)
            {
                flag=0;
                if( confirm("The movie name already exists. Replace it ?") )
                {
                    movArr.list[i].name = name;
                    movArr.list[i].rating = parseInt(rating);
                }
            }
        if(flag)
            movArr.list.push({name: name, rating: parseInt(rating)});
    }
    list();
}