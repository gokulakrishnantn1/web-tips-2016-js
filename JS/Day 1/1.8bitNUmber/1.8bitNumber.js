function startjs()
{
    var num = document.getElementById("num").value;
    var start = document.getElementById("start").value;
    var stop = document.getElementById("stop").value;
    var actual = document.getElementById("actual").value;
    if( num==="" || start==="" || stop==="" || actual==="" )
        alert("Fields cannot be empty !");
    else if( num>255 || num<0 )
        alert("Please enter an 8-bit number !");
    else if( start>7 || start<0 )
        alert("Please enter a valid start bit")
    else if( stop>7 || stop<0 )
        alert("Please enter a valid stop bit");
    else
    {
        if( stop<start )
        {
            var temp = start;
            start = stop;
            stop = temp;
        }
        if(actual>1)
            actual=1;
        var bin = (num >>> 0).toString(2);
        var arr = (""+bin).split("");
        arr = arr.reverse();
        for( var i=0; i<arr.length; i++)
        {
            if( arr[i]==1)
                arr[i]=1;
            else
                arr[i]=0;
        }
        for( var i=arr.length; i<8; i++)
            arr.push(0);
        for( var i=start; i<=stop; i++)
        {
            arr[i]=actual;
        }
        for( var i=0; i<arr.length; i++)
        {
            if( arr[i]==1)
                arr[i]=1;
            else
                arr[i]=0;
        }
        arr = arr.reverse();
        newArr = arr.toString();
        document.getElementById("content").innerHTML = "The new number is "+parseInt(newArr.replace(/,/g,''), 2)+" - ("+newArr.replace(/,/g,'')+")";
    }
}