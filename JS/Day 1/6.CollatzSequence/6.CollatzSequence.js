function startjs()
{
    var n = document.getElementById("num").value;
    if( n<1 || n%1!==0 )
        alert("Not a positive integer");
    else if( n==1 )
        document.getElementById("content").innerHTML = "<p>The stopping time is 0.</p>";
    else
    {
        var count = 0;
        do
        {
            if(n%2==0)
                n=n/2;
            else
                n=(n*3)+1;
            count++;
        }while(n>1);
        document.getElementById("content").innerHTML = "<p>The stopping time is "+count+"</p>";
    }
}